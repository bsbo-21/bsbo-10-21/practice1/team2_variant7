1.    Singleton. Был использован при создании объектов-контроллеров: EmployerManager, ReportController, TaskAssignmentController, WorkflowStatusTrackerController
      Цель применения: получение доступа к сущностям данных объектов, избегание сильных связей
      (Был внедрен в проект системы учета рабочего времени)

2.     Observer. Был использован при создании Employee(Observer), Assignment(Observer, Observable), Task(Observable)
      Цель применения: уведомление объектов об изменениях состояния статуса задач
      (Был внедрен в проект системы учета рабочего времени)

3.    Decorator. Представлен в виде отдельного проекта - DecoratorProject
