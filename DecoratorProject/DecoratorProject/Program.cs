﻿using System;

namespace DecoratorProject
{
    public class Program
    {
        public static void Main()
        {
            MathOperation operation1 = new Addition();
            MathOperation operation2 = new Multiplication();
            var result1 = operation1.Operation(3, 5); // Пока не происходит ничего интересного.
            var result2 = operation2.Operation(3, 5); //

            operation1 = new LogDecorator(operation1);
            operation2 = new LogDecorator(operation2);
            result1 = operation1.Operation(3, 5);  // Должно произойти автоматическое логирование.
            result2 = operation2.Operation(3, 5);  //

        }
    }

    public abstract class MathOperation
    {
        public string Name { get; private set; }

        protected MathOperation(string name)
        {
            Name = name;
        }

        public abstract float Operation(float x, float y);
    }

    public class Addition : MathOperation
    {
        public Addition() : base("Сложение")
        {
        }

        public override float Operation(float x, float y)
        {
            return x + y;
        }
    }

    public class Multiplication : MathOperation
    {
        public Multiplication() : base("Умножение")
        {
        }

        public override float Operation(float x, float y)
        {
            return x * y;
        }

    }

    public class LogDecorator : MathOperation
    {
        private readonly MathOperation _component;

        public LogDecorator(MathOperation component) : base(component.Name)
        {
            _component = component;
        }

        public override float Operation(float x, float y)
        {
            var result = _component.Operation(x, y);
            Console.WriteLine($"Проводится операция: {Name}({x}, {y}); Результат: {result}");
            return result;
        }
    }
}