﻿using System;
using System.Collections.Generic;
using Project.Utility;

namespace Project
{
    public class WorkflowStatusTrackerController
    {
        private Dictionary<Task, DateTime> timeForTask = new Dictionary<Task, DateTime>();

        private static WorkflowStatusTrackerController _instance;

        public static WorkflowStatusTrackerController instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new WorkflowStatusTrackerController();
                }

                return _instance;
            }
            private set => _instance = value;
        }
    
        private WorkflowStatusTrackerController() { }
    
        public void startTimeForTask(Task task)
        {
            Console.WriteLine("WorkflowStatusTrackerController.startTimeForTask(Task)");
            timeForTask.Add(task, DateTime.Now);
        }

        public Dictionary<Employee, string> getWorkflowStatus()
        {
            Console.WriteLine("WorkflowStatusTrackerController.getWorkFlowStatus()");
            EmployeeManager em = EmployeeManager.instance;
            var status = em.getEmployeesStatus();

            return status;
        }
    }
}