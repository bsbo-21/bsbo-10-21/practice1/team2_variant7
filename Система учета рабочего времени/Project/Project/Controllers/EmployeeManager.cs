using System.Collections.Generic;
using System;

namespace Project
{
    public class EmployeeManager
    {
        private List<Employee> employees = new List<Employee>();
        
        private static EmployeeManager _instance;

        public static EmployeeManager instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new EmployeeManager();
                    _instance.employees.Add(new Employee("Матвей"));
                }

                return _instance;
            }
        
            private set => _instance = value;
        }

        public Dictionary<Employee, string> getEmployeesStatus()
        {
            Console.WriteLine("EmployeeManager.getEmployeeStatus()");
            Dictionary<Employee, String> dictionary = new Dictionary<Employee, string>();
            
            foreach (var employee in employees)
            {
                dictionary[employee] = employee.getEmployeeStatus();
            }

            return dictionary;
        }

        public void addNewAssignment(Assignment assignment, Employee employee)
        {
            Console.WriteLine("EmployeeManager.addNewAssignment(Assignment, Employee)");
            employee.appointAssignment(assignment);

            sendNotification();
        }
        
        private void sendNotification(){
            Console.WriteLine("EmployeeManager.sendNotification()");
        }

        public Employee getEmployee(string name)
        {
            foreach (var employee in employees)
            {
                if (employee.name == name)
                    return employee;
            }

            return null;
        }
    }
}