
using System;

namespace Project
{
    public class TaskAssignmentController
    {
        private Assignment tempAssignment;
        private Employee tempEmployee;
        
        private static TaskAssignmentController _instance;

        public static TaskAssignmentController instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new TaskAssignmentController();
                }

                return _instance;
            }
        
            private set => _instance = value;
        }
        
        public void makeNewAssignment(string name, string empName)
        {
            Console.WriteLine("TaskAssignmentController.makeNewAssignment(string, Employee)");
            Employee employee = EmployeeManager.instance.getEmployee(empName);
            Assignment assignment = new Assignment();
            assignment.AddObserver(employee);
            tempAssignment = assignment;
            tempEmployee = employee;
            
            assignment.create(name, employee);
        }

        public void makeNewTask(string description)
        {
            Console.WriteLine("TaskAssignmentController.makeNewTask(string)");
            if (tempAssignment != null)
            {
                tempAssignment.addTask(description);
            }
        }

        public void pushAssignment()
        {
            Console.WriteLine("TaskAssignmentController.pushAssignment()");
            if (tempAssignment != null && tempEmployee != null)
            {
                EmployeeManager.instance.addNewAssignment(tempAssignment, tempEmployee);
            }
        }
    }
}