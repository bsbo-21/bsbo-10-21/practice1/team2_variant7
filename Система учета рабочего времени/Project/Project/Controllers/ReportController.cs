﻿using System;

namespace Project
{
    public class ReportController
    {
        private static ReportController _instance;

        public static ReportController instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new ReportController();
                }

                return _instance;
            }
        
            private set => _instance = value;
        }
    
        public string makeReport()
        {
            Console.WriteLine("ReportController.makeReport()");
            WorkflowStatusTrackerController wfst = WorkflowStatusTrackerController.instance;
            var status = wfst.getWorkflowStatus().ToString();

            return status;
        }
    }
}
