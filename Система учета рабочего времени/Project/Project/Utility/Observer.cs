namespace Project.Utility
{
    public interface IObserver
    {
        void Update();
    }
    
    public interface IObservable
    {
        void AddObserver(IObserver o);
        void RemoveObserver(IObserver o);
        void NotifyObservers();
    }
}