using System.Collections.Generic;
using System;
using Project.Utility;

namespace Project
{
    public class Task: IObservable
    {
        public string description;
        private List<IObserver> observers = new List<IObserver>();

        private string status { get; set; }

        public Task(string description)
        {
            this.description = description;
        }

        public string getTaskStatus()
        {
            Console.WriteLine("Task.getTaskStatus()");
            return status;
        }

        public void startCalculatingStatus()
        {
            Console.WriteLine("Task.startCalculatingStatus()");
            if (status == "new status")
            {
                NotifyObservers();
            }
        }
        
        public void AddObserver(IObserver o)
        {
            observers.Add(o);
        }

        public void RemoveObserver(IObserver o)
        {
            observers.Remove(o);
        }

        public void NotifyObservers()
        {
            foreach (IObserver observer in observers)
                observer.Update();
        }
    }
}