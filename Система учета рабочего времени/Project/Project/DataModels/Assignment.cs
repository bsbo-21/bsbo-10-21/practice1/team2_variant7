using System.Collections.Generic;
using System;
using Project.Utility;

namespace Project
{
    public class Assignment: IObservable, IObserver
    {
        public string name;
        public string status;
        public List<Task> tasks = new List<Task>();
        public List<IObserver> observers = new List<IObserver>();

        public static void startTrackingTask(Task task)
        {
            Console.WriteLine("Assignment.startTrackingTask(Task)");
            task.startCalculatingStatus();
        }

        public string getAssignmentStatus(Assignment assignment)
        {
            Console.WriteLine("Assignment.getAssignmentStatus(Assignment)");
            return assignment.status;
        }

        public void create (string name, Employee employee)
        {
            Console.WriteLine("Assignment.create(name, Employee)");
            this.name = name;
        }

        public void addTask (string description)
        {
            Console.WriteLine("Assignment.addTask(string)");
            Task task = new Task(description);
            task.AddObserver(this);
            tasks.Add(task);
        }

        public void AddObserver(IObserver o)
        {
            observers.Add(o);
        }

        public void RemoveObserver(IObserver o)
        {
            observers.Remove(o);
        }

        public void NotifyObservers()
        {
            foreach (IObserver observer in observers)
                observer.Update();
        }

        public void Update()
        {
            NotifyObservers();
        }
    }
}