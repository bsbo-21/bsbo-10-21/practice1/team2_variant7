using System.Collections.Generic;
using System;
using System.Linq;
using Project.Utility;

namespace Project
{
    public class Employee: IObserver
    {
        private List<Assignment> assignments = new List<Assignment>();
        public readonly string name;
        private string status = "";

        public Employee(string name)
        {
            this.name = name;
        }

        public string getEmployeeStatus()
        {
            Console.WriteLine("Employee.getEmployeeStatus()");
            return status;
        }

        public void appointAssignment(Assignment assignment)
        {
            Console.WriteLine("Employee.appointAssignment(Assignment)");
            assignments.Add(assignment);
        }

        public void Update()
        {
            string _newCalculatedStatus = "";
            status = _newCalculatedStatus;
        }
    }
}