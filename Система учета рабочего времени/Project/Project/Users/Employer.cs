using System;

namespace Project
{
    public class Employer: User
    {
        private void getReport()
        {
            Console.WriteLine("Employer.getReport()");
            ReportController.instance.makeReport();
        }

        private void createAssignment(string name, string empName)
        {
            Console.WriteLine("Employer.createAssignment(string, Employee)");
            TaskAssignmentController.instance.makeNewAssignment(name, empName);
        }
    }
}