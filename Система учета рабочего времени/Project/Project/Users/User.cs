﻿namespace Project
{
    public class User
    {
        private string name;
        private int id;
    
        public User(string name, int id)
        {
            this.name = name;
            this.id = id;
        }

        public User()
        {
            this.name = "";
            this.id = -1;
        }
    }
}

