﻿using System;

namespace Project
{
    internal class Program
    {
        public static void Main()
        {
            ReportController rc = ReportController.instance;
            TaskAssignmentController tac = TaskAssignmentController.instance;
            WorkflowStatusTrackerController wstc = WorkflowStatusTrackerController.instance;
            
            var running = true;
            while (running)
            {
                Console.WriteLine("\n\nТестирование системы учета рабочего времени. Выберите инцидент для тестирования:");
                Console.WriteLine("1. Выдача задания");
                Console.WriteLine("2. Отслеживание выполнения задания");
                Console.WriteLine("3. Учет затраченного на задание времени\n\n");
                
                var option = Console.ReadLine();
                
                
                
                switch (option)
                {
                    case "1":
                        tac.makeNewAssignment("assignment", "Матвей");
                        tac.makeNewTask("task 1");
                        tac.pushAssignment();
                        break;

                    case "2":
                        rc.makeReport();
                        break;

                    case "3":
                        Task task = new Task("task ?");
                        wstc.startTimeForTask(task);
                        Assignment.startTrackingTask(new Task("task ?"));
                        break;

                    default:
                        Console.WriteLine("Неверный ввод.");
                        running = false;
                        break;
                }

                Console.ReadLine();
                Console.Clear();
            }
        }
    }
}