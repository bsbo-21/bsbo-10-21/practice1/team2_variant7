```plantuml
@startuml

actor "Руководитель" as employer
participant TaskAssignmentController
participant Assignment
participant EmployeeManager
participant Employee

employer -> TaskAssignmentController : makeNewAssignment(Name, Emp)
TaskAssignmentController -> Assignment : create(Name, Emp)
loop 
  employer -> TaskAssignmentController : makeNewTask(Task)
  TaskAssignmentController -> Assignment : addTask(Task)
end
employer -> TaskAssignmentController : pushAssignment()
TaskAssignmentController -> EmployeeManager : addNewAssignment(Assignment)
EmployeeManager -> Employee : appointAssignment(Assignment)

@enduml
```
