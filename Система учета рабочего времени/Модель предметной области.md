```plantuml
@startuml

abstract class User {
  - name: String
  - id: UUID
}

class Employee {
  - assignments: [Assignment]
  + getEmployeeStatus()
  + appointAssignment(Assignment)
}

class Employer {
  - createAssignment(Name, Employee)
  - getReport()
}

class Assignment {
  - name: String
  - status: String
  - tasks: [Task]
  + startTrackingTask(Task)
  + getAssignmentStatus()
  + create(Name, Employee)
  + addTask(Task)
}

class Task {
  - description: String
  - status: String
  + startCalculatingStatus()
  + getTaskStatus()
}

class ReportController {
  + makeReport()
}

class TaskAssignmentController {
  + makeNewAssignment(Name, Employee)
  + makeNewTask(description)
  + pushAssignment()
}

class WorkflowStatusTrackerController {
  - timeForTask: [Task: Date]
  + startTimeForTask(Task)
  + getWorkflowStatus()
}

class EmployeeManager {
  - employees: [Employee]
  + getEmployeesStatus()
  + addNewAssignment(Assignment, Employee)
  + sendNotification()
}

User <|-- Employee
User <|-- Employer
Employee o-- Assignment
Assignment o-- Task
Employee "0..*" -- "1" WorkflowStatusTrackerController : count time for tasks
WorkflowStatusTrackerController "1" -- "1" EmployeeManager : get employees status
ReportController "1" -- "1" WorkflowStatusTrackerController: receiv workflow status
TaskAssignmentController "1" -- "1" EmployeeManager: appoint assignments for employees
TaskAssignmentController "1" -- "0..*" Employer : creat new assignments
ReportController "1" -- "0..*" Employer : receiving reports
WorkflowStatusTrackerController ..> Task
EmployeeManager o-- Employee
Employer "0..*" -- "1" EmployeeManager : notifies

@enduml
```
